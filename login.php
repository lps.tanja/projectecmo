<?php
require_once('https.php');
session_start();
require('db.php');


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="stylesheet.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<title>ECMO | Logga in</title>
<script defer>
		
	function showNotSaved() {
		alert("FEL! \n \n Ogiltigt användarnamn eller lösenord!");	
	}

</script>
</head>
<?php
if($_POST) {
	$username = mysqli_real_escape_string($connection,$_POST["username"]);
	$query = "SELECT pwd FROM userpwd WHERE user like '$username';";
	$query2 = "SELECT user FROM userpwd WHERE user like *";
	
	if	(mysqli_query($connection,$query2)) {
	} else {
	echo '<script>showNotSaved();</script>';
	}
	
	if($result = mysqli_query($connection,$query)){
   	while ($row = mysqli_fetch_assoc($result)) {
      	if (password_verify($_POST["pwd"], $row["pwd"])) {
				$_SESSION["username"]= $_POST["username"];
				header('Location: ecmo.php'); 
				exit; 
			} 	else {
			echo '<script>showNotSaved();</script>';
			}	
		}
 	} else {
 	echo '<script>showNotSaved();</script>';
	}
}
?>

<body>
<header>
	<h1>ECMO-Centrum</h1>
	<h3>Patientens Transportdokument	</h3>
	<div class="tablet" title="Mobilvisning"><i class="fas fa-tablet-alt"></i></div>
	<div class="computer" title="Datorvisning"><i class="fas fa-laptop"></i></div>
</header>

<div class="login">
<h2>Logga in</h2><br>
<form action="<?= htmlentities($_SERVER['SELF']) ?>" method="post">
	Användarnamn <br><input type="text" name="username" value="" placeholder="användarnamn..." required autofocus><br>
	Lösenord <br><input type="password" name="pwd" value="" placeholder="lösenord..." required> <br>
	<input type="submit" value="Logga in">
</form>
</div>

<footer>
	<img class="image" src="img/boysndrca.png" width="100px" height="100px"><br>
	<a>Sida gjord av lag: Boys N Drca</a>
</footer>
</body>
</html>