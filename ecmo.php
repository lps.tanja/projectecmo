<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="stylesheet.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<title>ECMO | dokument</title>


<script defer>
	function showSaved() {
		alert("Ny användare skapades framgångsrikt!");	
	}
	
	function showNotSaved() {
		alert("FEL! \nFel när du skapar ny användare!");	
	}
</script>

</head>

<?php
require_once('https.php');
session_start();
require('db.php');

if($_SESSION["username"]) {
} else {
	header("Location: login.php");
	exit;
}	

if ($connection === false) {
	mysqli_connect_error();
	exit;
} else {
}
/* Create new user with hashed password */
if($_POST) { 
	$newusername = $_POST["newusername"];
	$newpassword = password_hash($_POST["newpassword"], PASSWORD_DEFAULT);
	$sql = "INSERT INTO userpwd (user, pwd) VALUES ('$newusername', '$newpassword')";

	if(mysqli_query($connection, $sql)) {
		echo '<script>showSaved();</script>';
	} else {
		echo '<script>showNotSaved();</script>';
	}
}
?>	

<body>
<header>
	<a href="#logoutwarning" onclick="resetLogout()" id="logout"><i class="fas fa-sign-out-alt" title="Logga ut"></i><br><br>Logga ut</a>
	<h1>ECMO-Centrum</h1>
	<h3>Patientens Transportdokument</h3>
		<div class="tablet" title="Mobilvisning"><i class="fas fa-tablet-alt"></i></div>
		<div class="computer" title="Datorvisning"><i class="fas fa-laptop"></i></div>
</header>

<div class="logout" id="logoutwarning">
	<h2>Anmärkning!</h2>
	<h4>Är du säkert logga ut?</h4>
	<a class="button" onclick="removeLogout()"><i class="far fa-times-circle"></i><br>Nej</a>
	<a href="logout.php"><i class="fas fa-sign-out-alt" title="Logga ut"></i><br>Logga ut</a>
</div>

<div class="col-4">
<a href="index.php" class="button1"><br><br><i class="fas fa-file-medical" id="but1"></i><br><br><h3>Ny</h3>ECMO-patientens<br>transportdokument</a>
</div>
<div class="col-4">
<a href="" class="button2"><br><br><i class="fas fa-history" id="but2"></i><br><br><h3>Historia</h3>ECMO-patientens<br>transportdokument</a>
</div>

<div class="col-4">
<div class="createuser">
	<i class="fas fa-user"></i><h3>Skapa en ny användare</h3>
	<form action="<?= htmlentities($_SERVER['SELF']) ?>" method="post">
		<input type="text" name="newusername" placeholder="Nytt användarnamn..." pattern=".{4,}" title="Fyra eller flera tecken" required><br> 
		<input type="password" name="newpassword" placeholder="Nytt lösenord..." pattern=".{4,}" title="Fyra eller flera tecken" required><br> 
		<input type="submit" value="LÄMNA" title="Lämna nytt användarnamn och lösenord">
	</form>
</div>
</div>


<footer>
	<img src="img/boysndrca.png"><br>
	<a>Sida gjord av lag: Boys N Drca</a>
</footer>

<script type="text/javascript">

	function removeLogout() {
	document.getElementById("logoutwarning").style.display = "none";	
	}
	
	function resetLogout() {
	document.getElementById("logoutwarning").style.display = "block";	
	} 

</script>

</body>
</html>