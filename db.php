<?php
/*This file is used to create connection on each site for database
Connect to database with this in ->>>>> require(db.php); */

$config = parse_ini_file('config.ini');
$connection = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if ($connection === false) {
	echo "Error connecting to database<br>Virhe: " .
	mysqli_connect_error();
	exit;
}

?>